(function(compId) {
    var _ = null,
        y = true,
        n = false,
        x11 = '1600px',
        x2 = '5.0.0',
        o = 'opacity',
        x9 = 'rgb(0, 0, 0)',
        x13 = 'auto',
        e7 = '${Symbol_default}',
        x14 = 'rgba(0,0,0,1.00)',
        x12 = '698px',
        x6 = 'rgba(255,255,255,1)',
        x3 = '5.0.1.386',
        m = 'rect',
        x10 = '0px',
        x8 = 'Rectangle',
        x1 = '5.0.1',
        i = 'none';
    var g4 = 'loading.mp4';
    var im = 'images/',
        aud = 'media/',
        vid = 'media/',
        js = 'js/',
        fonts = {},
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [],
        scripts = [],
        symbols = {
            "stage": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        id: 'loading',
                        volume: '0',
                        t: 'video',
                        tag: 'video',
                        r: ['0px', '0px', '1600px', '900px', 'auto', 'auto'],
                        ap: 'autoplay',
                        lp: 'loop',
                        sr: [vid + g4],
                        pr: 'auto'
                    }, {
                        id: 'lq2',
                        t: 'video',
                        tag: 'video',
                        r: ['0', '900px', '1600px', '900px', 'auto', 'auto'],
                        ap: 'autoplay',
                        lp: 'loop',
                        sr: [vid + g4],
                        pr: 'auto'
                    }, {
                        id: 'Symbol_default',
                        symbolName: 'Symbol_default',
                        t: m,
                        r: ['0', '1222', '1600px', '698px', 'auto', 'auto'],
                        o: '1'
                    }],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            r: ['null', 'null', '1600px', '1920px', 'auto', 'auto'],
                            overflow: 'hidden',
                            f: [x6]
                        }
                    }
                },
                tt: {
                    d: 2000,
                    a: y,
                    data: [
                        ["eid2", o, 0, 2000, "linear", e7, '1', '0']
                    ]
                }
            },
            "Symbol_default": {
                v: x1,
                mv: x2,
                b: x3,
                stf: i,
                cg: i,
                rI: n,
                cn: {
                    dom: [{
                        t: m,
                        id: x8,
                        s: [0, x9, i],
                        r: [x10, x10, x11, x12, x13, x13],
                        f: [x14]
                    }],
                    style: {
                        '${symbolSelector}': {
                            r: [_, _, x11, x12]
                        }
                    }
                },
                tt: {
                    d: 0,
                    a: y,
                    data: []
                }
            }
        };
    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);
})("EDGE-default");
(function($, Edge, compId) {
    var Composition = Edge.Composition,
        Symbol = Edge.Symbol;
    Edge.registerEventBinding(compId, function($) {

        (function(symbolName) {
            Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
                sym.$("loading")[0].play();
                sym.$("lq2")[0].play();
            });

            Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2000, function(sym, e) {
                if (!sym.getSymbol("Symbol_default").isPlaying()) {
                    sym.getSymbol("Symbol_default").play(start);
                }
            });

        })("stage");

        (function(symbolName) {})("Symbol_default");

    })
})(AdobeEdge.$, AdobeEdge, "EDGE-default");
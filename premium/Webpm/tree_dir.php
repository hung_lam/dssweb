<?php
$json_data = readJsonFile();
$arr_json_data = json_decode($json_data,true);
function readJsonFile(){
	return file_get_contents(getcwd().'/datas/campaign.json');
}
function php_file_tree($directory, $return_link, $extensions = array(),$short_line = "play_Google_Drive",$cloudStoreName="Google Drive") {	
	if( substr($directory, -1) == "/" ) $directory = substr($directory, 0, strlen($directory) - 1);
	$code = php_file_tree_dir($directory, $return_link, $extensions,true,$short_line ,$cloudStoreName );	
	return $code;
}

function php_file_tree_dir($directory, $return_link, $extensions = array(), $first_call = true,$short_line = "play_Google_Drive",$cloudStoreName="Google Drive") {	
	$default_dir = explode($short_line, $directory);
	if(isset($default_dir[1] ) ){
		$path = $short_line.$default_dir[1];
	}else{
		$path = $directory;
	}	
	if( function_exists("scandir") ) $file = scandir($directory); else $file = php4_scandir($directory);
	natcasesort($file);
	$files = $dirs = array();
	foreach($file as $this_file) {
		if( is_dir("$directory/$this_file" ) ) $dirs[] = $this_file; else $files[] = $this_file;
	}	
	$file = array_merge($dirs, $files);
	
	if( !empty($extensions) ) {
		foreach( array_keys($file) as $key ) {
			if( !is_dir("$directory/$file[$key]") ) {
				$ext = substr($file[$key], strrpos($file[$key], ".") + 1); 
				if( !in_array($ext, $extensions) ) {
					unset($file[$key]);
				}
			}
		}
	}
	$php_file_tree = "";
	if( count($file) > 2 ) { 
		$php_file_tree = "<ul";
		if( $first_call ) { $php_file_tree .= " class=\"php-file-tree\""; $first_call = false; }
		$php_file_tree .= ">";
		foreach( $file as $this_file ) {
			if( $this_file != "." && $this_file != ".." ) {
				if( is_dir("$directory/$this_file") ) {
					$php_file_tree .= "<li class=\"pft-directory\"><a href=\"#\">" . htmlspecialchars($this_file) . "</a>";
					$php_file_tree .= php_file_tree_dir("$directory/$this_file", $return_link ,$extensions, false,$short_line ,$cloudStoreName);
					$php_file_tree .= "</li>";
				} else {
					$ext = "ext-" . substr($this_file, strrpos($this_file, ".") + 1); 
					$link = str_replace("[link]", $path."/" . urlencode($this_file), $return_link);
					$link = str_replace("[cloudname]", $cloudStoreName,$link);
					$php_file_tree .= "<li class=\"pft-file " . strtolower($ext) . "\"><a href=\"$link\">" . htmlspecialchars($this_file) . "</a></li>";
				}
			}
		}
		$php_file_tree .= "</ul>";
	}
	return $php_file_tree;
}
function php4_scandir($dir) {
	$dh  = opendir($dir);
	while( false !== ($filename = readdir($dh)) ) {
	    $files[] = $filename;
	}
	sort($files);
	return($files);
}
function write_file($data){
	$fp = fopen("xem.txt","a+");
	fwrite($fp,$data);
	fclose($fp);
}

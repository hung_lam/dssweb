function calSizeFitBox(slide_w,slide_h,box_w,box_h){
    var ret = {};
    if(box_w/box_h > slide_w/slide_h){ //fit width
        ret['w'] = box_w;
        ret['h'] = Math.round(slide_h/(slide_w/box_w));
        ret['scale'] = slide_w/box_w;
        ret['scale_by'] = 'W';
    }else{
        ret['w'] = Math.round(slide_w/(slide_h/box_h));
        ret['h'] = box_h;
        ret['scale'] = slide_h/box_h;
        ret['scale_by'] = 'H';
    }
    return ret;
}
function CheckRotate(w,h){
  var device = Orientation($(window).width(),$(window).height());
  var slide  = Orientation(w,h);
  if(device==slide)
    return false;
  else
    return true;
}
function Orientation(w,h){
    if(w>h) return 'v'; else return 'h';
}
function launchIntoFullscreen(element) {
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if(element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}
function exitFullscreen() {
  if(document.exitFullscreen) {
    document.exitFullscreen();
  } else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}

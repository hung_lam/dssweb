<?php

function addArrayFiles($filename,$dir){
    $arr_tmp = array();
    $arr_tmp['filename'] = $filename;
    $arr_tmp['folder'] = $dir;
    $arr_tmp['modified_date'] = filemtime($dir."/".$filename);
    $arr_tmp['file_size'] = filesize($dir."/".$filename);
    $arr_tmp['modified_to_date'] = date ("F d Y H:i:s.", filemtime($dir."/".$filename) );
    return $arr_tmp;
}
function GetInfoFiles($dir,$arr,$mod="../sync"){
    $files = scandir($dir, 1);
    foreach ($files as $key => $value) {
        $count = count(explode(".", $value));
        if($value=="." || $value==".." || $value=="desktop.ini"){
        }else if($count==1){
            $subdir = $dir."/".$value;
            $arr = GetInfoFiles($subdir,$arr,$mod);
        }else{
            $keyfile = str_replace(" ","_",$dir."/".$value);
            $keyfile = str_replace($mod."/","",$keyfile);
            $arr[$keyfile] = addArrayFiles($value,$dir);
        }
    }
    return $arr;
}
$cloudResource = file_get_contents('../../../cloud.txt');
$sync_folder = '../../../'.($cloudResource!=''?$cloudResource:'') . '/sync';
if(!file_exists(getcwd()."/play")) @mkdir(getcwd()."/play",0777);
$backup_folder = getcwd()."/play/backup";
if(!file_exists($backup_folder)) @mkdir($backup_folder,0777);
if(!file_exists(getcwd()."/play/media")) @mkdir(getcwd()."/play/media",0777);
$arr_sync = GetInfoFiles($sync_folder,array(),$sync_folder);
$arr_play = GetInfoFiles("play",array(),"play");
$n=0;
foreach ($arr_sync as $key => $value) {
    
    $filename = $value['folder']."/".$value['filename'];
    $path = str_replace($sync_folder, "play",$filename);
    $arr_path = explode("/", $path);
    $new_path = '/';
    for($i=0;$i<count($arr_path)-1;$i++){
        $new_path .=$arr_path[$i]."/";
        if(!file_exists(getcwd().$new_path)) @mkdir(getcwd().$new_path,0777);
    }
    $new_index = str_replace("Google_Drive", "Google Drive",$filename);
    $new_index = str_replace("One_Drive", "OneDrive",$new_index);
    $new_index = str_replace("Drop_box", "Dropbox",$new_index);
    $new_index = str_replace($sync_folder."/", "",$new_index);    
    $new_index = str_replace(" ", "_",$new_index);        

    $check_replace = str_replace($sync_folder, "play",$filename);      
    if( !isset($arr_play[$new_index]) || (int)$value['file_size'] != (int)$arr_play[$new_index]['file_size'] || (int)$value['modified_date']>(int)$arr_play[$new_index]['modified_date']){
        if(file_exists(getcwd().'/'.$check_replace)) {
            $folder_date = date('d-m-y');
            $backup_folder = $backup_folder.'/'.$folder_date;
            @mkdir($backup_folder,0777);
            $backup_folder .= '/play';
            @mkdir($backup_folder,0777);
            $v_new_file_play = getcwd().'/'.str_replace('play', "/play/backup/".$folder_date.'/pla',$check_replace);            
            @rename(getcwd().'/'.$check_replace, $v_new_file_play);
        }        
        copy($filename,str_replace($sync_folder, "play",$filename));        
        $n++;
    }
}
function write_file($text)
{
    $fp = fopen(getcwd().'/xem.txt','a+');
    fwrite($fp,$text);
    fclose($fp);
}
echo $n;
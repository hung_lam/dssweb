$(".input_file span.btn").on("click",function(e){
	e.preventDefault();
	$(this).next().click();
})
$(".input_file input[type=file]").on("change",function(){
	value = $(this).val();
	text = value.replace('/','\\');
	text = value.split('\\');
	text = text[text.length-1];
	$(this).prev().text(text);
	$(this).prev().prop('title',text);
})
$(".input_range input[type=text]").each(function(){
	$(this).val($(this).prev().val()+"%");
})
$(".input_range input[type=range]").on("mousemove",function(){
	$(this).next().val($(this).val()+"%");
})
// Var
var Clock,ClockCheckConnetion;
var point = 0;
var ConnetionStartus = false;
var Setting;
var ClockDownload, Downloading;
var CheckSyncing;
var DelayTime =0;
var delayCheck = 5000;
var countNew=0;
var check_count = -1;
var update_to_date_data = false;
Bootrap();
function Bootrap(){
  LoadSetting();
  CheckAndCopySysnFiles();
  LoadIndex();
}
function LoadSetting(){
  var d = new Date(); var ts = "?t="+d.getTime();
  $.getJSON("datas/bootstrap.json"+ts, function(result){    
      Setting = result;      
      if(Setting.ResourceFolder!=undefined){
        if(check_count>=0) SetSize();
        startTime();
      }
  });
}
function SetSize(){
  var h,w,newsize,margin;
  var isRotate = false;
  if(Setting.SizeMode!=undefined && Setting.SizeMode=="custom" && Setting.BannerWidth!=undefined && Setting.BannerHeight!=undefined){
      w = Setting.BannerWidth;
      h = Setting.BannerHeight;
  }else{
      w = $(window).width();
      h = $(window).height();
  }
  if(CheckRotate(w,h)){
      var tmp=w; w=h; h=tmp;
      isRotate = true;
  }
  var newsize = calSizeFitBox(w,h,$(window).width(),$(window).height());
  w = newsize['w'];
  h = newsize['h'];
  var margin = 0;
  if(isRotate){
    var tmp=w; w=h; h=tmp;
    margin = Math.round((Math.max(w,h) - Math.min(w,h))/2);
    $("#mainFrame").addClass("container_rotate_90_negative");
  }
  $("#mainFrame").attr("width",w);
  $("#mainFrame").attr("height",h);
  $("#mainFrame").css("width",w+"px");
  $("#mainFrame").css("height",h+"px");
  $("#mainFrame").css("margin-left",margin+"px");
  $("#mainFrame").css("margin-top","-"+margin+"px");
}
function LoadIndex(){
  if(check_count>=0){
    var d = new Date(); var ts = "?t="+d.getTime();
    $("#mainFrame").removeClass('default_frame');
    $("#mainFrame").attr("src","play/index.html?t="+ts);
  }    
}
function calSizeFitBox(slide_w,slide_h,box_w,box_h){
    var ret = {};
    if(box_w/box_h > slide_w/slide_h){ //fit width
        ret['w'] = box_w;
        ret['h'] = Math.round(slide_h/(slide_w/box_w));
        ret['scale'] = slide_w/box_w;
        ret['scale_by'] = 'W';
    }else{
        ret['w'] = Math.round(slide_w/(slide_h/box_h));
        ret['h'] = box_h;
        ret['scale'] = slide_h/box_h;
        ret['scale_by'] = 'H';
    }
    return ret;
}
function CheckRotate(w,h){
  var device = Orientation($(window).width(),$(window).height());
  var slide  = Orientation(w,h);
  if(device==slide)
    return false;
  else
    return true;
}
function Orientation(w,h){
    if(w>h) return 'v'; else return 'h';
}
function launchIntoFullscreen(element) {
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if(element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}
function exitFullscreen() {
  if(document.exitFullscreen) {
    document.exitFullscreen();
  } else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}
function startTime() {
    //Count Main
    if(DelayTime>parseInt(Setting.DownloadTime)){
        DelayTime = 0;
    }
    DelayTime++;
    $("#clock_time").css("display","block");
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    // add a zero in front of numbers<10
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById("clock_time").innerHTML = h + ":" + m + ":" + s;
    var t = setTimeout(function(){ startTime() }, 500);
}
function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
function CheckAndCopySysnFiles(){
  if(delayCheck==undefined || delayCheck<1000)
      delayCheck = 5000;
  console.log(ConnetionStartus,"Checking Sync Files ..............",DelayTime);
  if(!CheckSyncing && DelayTime<100){
    CheckAndCopyAjax();
  }
  ClockDownload = setTimeout(function(){
      CheckAndCopySysnFiles();
  },delayCheck);  
}
function CheckAndCopyAjax(){
  if(Setting == undefined) return ;
  CheckSyncing = true;  
  console.log(ConnetionStartus," Check And Copy Sysn Files ..............");  
  $.ajax({
      method: "POST",
      url: "check_and_copy_sync_files.php",
      data: {
        type: 'auto'
        ,'cloudResource':Setting['cloudResource']
      },
      success:function(ret){        
        console.log('check copy sync file');
        console.log(ret);
        if(parseInt(ret)>0){
          $("#mainFrame").attr('src','/web/default_play/index.html');
          $("#mainFrame").addClass('default_frame');
          update_to_date_data = true;
          check_count = 1;
          Bootrap();
        }else{
          check_count = 0;
          if(update_to_date_data==false) {
            update_to_date_data = true;
            Bootrap();
          }
        }
        CheckSyncing = false;
      }
  });
}

<?php 
	ob_start();
	include ("tree_dir.php");
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8"/>
		<title>Timeline</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport"/>
		<meta content="" name="jt-db"/>
		<link href="/Webpm/css/application.css" rel="stylesheet" type="text/css" id="style-color"/>
		<link href="/Webpm/css/components.css" id="style-components" rel="stylesheet" type="text/css"/>
	    <link href="/Webpm/css/default.css" rel="stylesheet" type="text/css" id="style-color"/>
	    <link href="/Webpm/css/layout.css" rel="stylesheet" type="text/css"/>
	    <link href="/Webpm/css/custom.css" rel="stylesheet" type="text/css"/>
	    <link href="/Webpm/css/tree_dir.css" id="style-components" rel="stylesheet" type="text/css"/>

	    <link href="/Webpm/css/bootstrap-combined.min.css" rel="stylesheet" type="text/css"/>
	    <link href="/Webpm/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css"/>

	    <script src="/Webpm/js/jquery.js" type="text/javascript"></script>
	    <script src="/Webpm/js/php_file_tree.js" type="text/javascript"></script>

	    <script src="/Webpm/js/bootstrap-datetimepicker.min-custom.js" type="text/javascript"></script>

	    <script src="/Webpm/js/jquery-ui-1.9.2.custom.js" type="text/javascript"></script>

	    <script src="/Webpm/js/jquery.ui.sortable.js" type="text/javascript"></script>

	    <script src="/Webpm/js/svg.js" type="text/javascript"></script>

	    <script src="/Webpm/js/svg.draggable.js" type="text/javascript"></script>	    
	</head>
	<body class="page-header-fixed page-header-fixed-mobile page-quick-sidebar-over-content">
		<div class="page-container">
			<div class="page-sidebar-wrapper">
				<div class="page-sidebar navbar-collapse collapse">
					<ul id="sidebar-menu" class="page-sidebar-menu page-sidebar-menu-default" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">					
						<li>
							<a href="javascript:void(0)">
								<span class="title">Sequencer</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)">
								<span class="title">Scheduler</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="page-content-wrapper">
				<div class="page-content">
					<h3 class="page-title">
						Sequencer
					</h3>					
					<div class="row">
					    <div class="col-md-12">
					        <div class="portlet">
					            <div class="portlet-title">					                  
					                <form method="POST" accept-charset="UTF-8" class="form-horizontal" id="layout-form">					                
					                    <div class="actions" style="">                   
					                        <button data-id="resouces_gd" type="button" onclick="click_btn(this);" class="btn default yellow-stripe " data-toggle="modal">
					                            <i class="fa fa-plus"></i>
					                            <span class="hidden-480">Google Drive</span>
					                        </button>

					                        <button data-id="resouces_od" type="button" onclick="click_btn(this);" class="btn default yellow-stripe" data-toggle="modal">
					                            <i class="fa fa-plus"></i>
					                            <span class="hidden-480">One Drive</span>
					                        </button>

					                        <button data-id="resouces_db" type="button" onclick="click_btn(this);" class="btn default yellow-stripe active" data-toggle="modal">
					                            <i class="fa fa-plus"></i>
					                            <span class="hidden-480">Dropbox</span>
					                        </button>
					                    </div>					                   
					                </form>

					            </div>					            
					            <div class="portlet-body">
					                <div class="col-md-3">
					                	<div class="col-md-12" style="margin-bottom:1px solid gray">
					                		<h3>Resources</h3>
					                	</div>
					                    <div class="col-md-12 resources_div" id="resouces_gd" style="display:none">
					                    	<?php
					                    		if(file_exists(getcwd()."/play_Google_Drive/")) 
					                    			echo php_file_tree($_SERVER['DOCUMENT_ROOT']."/Webpm/play_Google_Drive", "javascript:click_ne('[link]','[cloudname]');",array('html'));
					                    		else echo '<div>Folder not found </div>';
					                    	?>
					                    </div>
					                    <div class="col-md-12 resources_div" id="resouces_od" style="display:none;">
					                    	<?php
					                    		if(file_exists(getcwd()."/play_OneDrive/")) 
					                    			echo php_file_tree($_SERVER['DOCUMENT_ROOT']."/Webpm/play_OneDrive", "javascript:click_ne('[link]','[cloudname]');",array('html'),"play_OneDrive","OneDrive"); 
					                    		else echo '<div>Folder not found </div>';					                    		
					                    	?>
					                    </div>
					                    <div class="col-md-12 resources_div" id="resouces_db" style="">
											<?php 
												if(file_exists(getcwd()."/play_Dropbox/")) 
					                    			echo php_file_tree($_SERVER['DOCUMENT_ROOT']."/Webpm/play_Dropbox", "javascript:click_ne('[link]','[cloudname]');",array('html'),"play_Dropbox","Dropbox");
					                    		else echo '<div>Folder not found </div>';					                    							                    
					                    	 ?>
					                    </div>
					                </div>
					                <div class="col-md-9">
					                	<div class="col-md-12" style="margin-bottom:1px solid gray">
					                		<h3>Information</h3>
					                	</div>
				                		<form method="POST" accept-charset="UTF-8" class="form-horizontal">
				                			<div class="col-md-12">
					                			<div class="col-md-3">
					                				Pending:
					                			</div>
					                			<div class="col-md-8">
													<div id="datetimepicker3" class="input-append">
														<span class="add-on">
															<i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-time">
															</i>
														</span>
														<input type="text" class="form-control" id="pending_time" data-format="hh:mm:ss"  />
													</div>					                				
					                			</div>

					                			<div class="col-md-4">
					                				Link:
					                			</div>
					                			<div class="col-md-8">
					                				<span id="link">&nbsp;</span>
					                				<input type="hidden" id="link_hidden" value="" />
					                			</div>

					                			<div class="col-md-4">
					                				Cloud storage name:
					                			</div>
					                			<div class="col-md-8">
					                				<span id="cloud_name">&nbsp;</span>
					                				<input type="hidden" id="cloud_name_hidden" value="" />
					                			</div>

					                			<div class="col-md-12 clear">
					                				<button id="add_record" type="button" class="btn green yellow-stripe" style="margin-top:20px" data-toggle="modal">
							                            <span class="hidden-480">Add</span>
							                        </button>
					                			</div>
					                		</div>
				                		</form>
				                		<div class="col-md-12 clear"> 
				                			<h3>List data</h3>
				                		</div>
				                		<div class="col-md-9">
				                			<table class="table table-striped table-hover table-bordered" id="list-data">
							                    <thead>
							                        <tr role="row" class="heading">
							                            <th>#</th>
							                            <th>Link</th>
							                            <th>Cloud</th>
							                            <th>Pending</th>
							                            <th>action</th>
							                        </tr>							                        
							                    </thead>
							                    <tbody>
							                    	<?php for($i=0;$i<=count($arr_json_data['timeline']);$i++){ if(!isset($arr_json_data['timeline'][$i])) continue; ?>
								                        <tr id="tr_<?php echo $i;?>" role="row" class="heading">
								                            <td><span id="td_<?php echo $i;?>"><?php echo $i;?></span></td>
								                            <td><?php echo $arr_json_data['timeline'][$i]['link'] ; ?></td>
								                            <td><?php echo $arr_json_data['timeline'][$i]['cloudName'] ; ?></td>
								                            <td>
								                            	<span id="span_<?php echo $i ;?>"><?php echo $arr_json_data['timeline'][$i]['pending'] ; ?></span>
								                            	<input class="form-control" type="text" id="pending_input_<?php echo $i ;?>" value="<?php echo $arr_json_data['timeline'][$i]['pending'] ; ?>" style="display:none;width:70px;" />
								                            </td>
								                            <td>
								                            	<button data-index="<?php echo $i ;?>" type="button" id="delete_<?php echo $i ;?>" class="delete_index btn green" style="text-align:right" data-toggle="modal">
										                            <span class="hidden-480">Delete</span>
										                        </button>
										                        <button data-index="<?php echo $i ;?>" type="button" id="edit_<?php echo $i ;?>" class="edit_index btn green" style="text-align:right" data-toggle="modal">
										                            <span class="hidden-480">Edit</span>
										                        </button>

										                        <button data-index="<?php echo $i ;?>" type="button" id="save_<?php echo $i ;?>" class="save_index btn blue" style="text-align:right;display:none" data-toggle="modal">
										                            <span class="hidden-480">Save</span>
										                        </button>

										                        <button data-index="<?php echo $i ;?>" type="button" id="cancel_<?php echo $i ;?>" class="cancel_index btn blue" style="text-align:right;display:none" data-toggle="modal">
										                            <span class="hidden-480">Cancel</span>
										                        </button>

								                            </td>
								                        </tr>							                        
							                        <?php  }?>
							                        <input type="hidden" id="current_index" value = "<?php echo isset($i)?$i : 0 ;?>" />
							                    </tbody>
							                </table>
							                <form method="post" action="json_data.php">
							                	<button type="submit" id="save_json" class="btn green yellow-stripe" style="margin-top:20px" data-toggle="modal">
						                            <span class="hidden-480">Save</span>
						                        </button>
						                        <input type="hidden" id="json_data" name="json_data">
							                </form>
				                		</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var data_data = JSON.parse('<?php echo json_encode($arr_json_data); ?>');
			var start_id,update_id ;
	    	function click_btn(obj){
	    		$('.resources_div').hide();
	    		$('#'+$(obj).attr("data-id")).show();
	    	}	    	
	    	function drag_drop_timline(){
		        $("#list-data tbody").sortable({
		            group: 'no-drop',
		            drag: false,
		            start: function (event, ui){
		            	start_id = ui.item[0].id;
		            	start_id = parseInt(start_id.replace("tr_",""),10);
		            },
		            update:function(event, ui){		            	
		            	if(ui.item[0].previousElementSibling) update_id = ui.item[0].previousElementSibling.id;
		            	else update_id = ui.item[0].nextElementSibling.id;
		            	update_id = parseInt(update_id.replace("tr_",""),10);		            			            	
		            },
		            stop:function(event, ui){
		            	console.log("start "+start_id);
		            	console.log("update "+update_id);
		            	var current_start = start_id;
		            	var current_update = update_id;
		            	var new_update_id = update_id+'---';
		            	$("#span_"+update_id).attr('id',"span_"+new_update_id);
		            	$("#pending_input_"+update_id).attr('id',"pending_input_"+new_update_id);
		            	$("#delete_"+update_id).attr('data-index',new_update_id);
		            	$("#delete_"+update_id).attr('id',"delete_"+new_update_id);
		            	$("#edit_"+update_id).attr('data-index',new_update_id);
		            	$("#edit_"+update_id).attr('id',"edit_"+new_update_id);
		            	$("#save_"+update_id).attr('data-index',new_update_id);
		            	$("#save_"+update_id).attr('id',"save_"+new_update_id);
		            	$("#cancel_"+update_id).attr('data-index',new_update_id);
		            	$("#cancel_"+update_id).attr('id',"cancel_"+new_update_id);
		            	$("#tr_"+update_id).attr('id',"tr_"+new_update_id);		
		            	$("#td_"+update_id).attr('id',"td_"+new_update_id);	    	
		            	//====
		            	$("#span_"+start_id).attr('id',"span_"+update_id);
		            	$("#pending_input_"+start_id).attr('id',"pending_input_"+update_id);
		            	$("#delete_"+start_id).attr('data-index',update_id);
		            	$("#delete_"+start_id).attr('id',"delete_"+update_id);
		            	$("#edit_"+start_id).attr('data-index',update_id);
		            	$("#edit_"+start_id).attr('id',"edit_"+update_id);
		            	$("#save_"+start_id).attr('data-index',update_id);
		            	$("#save_"+start_id).attr('id',"save_"+update_id);			            	
		            	$("#cancel_"+start_id).attr('data-index',update_id);
		            	$("#cancel_"+start_id).attr('id',"cancel_"+update_id);
		            	$("#tr_"+start_id).attr('id',"tr_"+update_id);
		            	$("#td_"+start_id).html(update_id);
		            	$("#td_"+start_id).attr('id',"td_"+update_id);
		            	//======
		            	$("#span_"+new_update_id).attr('id',"span_"+current_start);
		            	$("#pending_input_"+new_update_id).attr('id',"pending_input_"+current_start);
		            	$("#delete_"+new_update_id).attr('data-index',current_start);
		            	$("#delete_"+new_update_id).attr('id',"delete_"+current_start);
		            	$("#edit_"+new_update_id).attr('data-index',current_start);
		            	$("#edit_"+new_update_id).attr('id',"edit_"+current_start);
		            	$("#save_"+new_update_id).attr('data-index',current_start);
		            	$("#save_"+new_update_id).attr('id',"save_"+current_start);
		            	$("#cancel_"+new_update_id).attr('data-index',current_start);
		            	$("#cancel_"+new_update_id).attr('id',"cancel_"+current_start);
		            	$("#tr_"+new_update_id).attr('id',"tr_"+current_start);		            	
		            	$("#td_"+new_update_id).html(current_start);
		            	$("#td_"+new_update_id).attr('id',"td_"+current_start);
		            	//====

		                var timeline_data = data_data['timeline'];
		                var current = timeline_data[start_id];
		                timeline_data[start_id] = timeline_data[update_id];
		                timeline_data[update_id] = current;
		                data_data['timeline'] = timeline_data;
		            }
		        });
		    }
	    	$(function(){	    		
	    		drag_drop_timline();	    		
	    		
	    		$('#datetimepicker3').datetimepicker({
		          pickDate: false
		        }).datetimepicker('setDate',"00:00:30" );
	    		 
	    		$(".delete_index").on('click',function(){	    			
	    			var index_data = parseInt($(this).attr('data-index'),10);	    			
	    			var index = 1;
	    			var new_data = [];
	    			var timeline_data = data_data['timeline'];
	    			for(var i =1; i<=$("#current_index").val() ;i++){
	    				if(timeline_data[i]=='undefined' || !timeline_data[i]) continue;
	    				else{
	    					if(index_data!= i){
	    						new_data[index] = timeline_data[i];
	    						index++;
	    					}
	    				}	    				
	    			}
	    			data_data['timeline'] = new_data;
	    			$("#tr_"+index_data).remove();
	    		});

	    		$("#save_json").on('click',function(){
	    		 	$("#json_data").val(JSON.stringify(data_data));
	    		});
	    		
	    		$(".save_index").on('click',function(){

	    			var index_data = parseInt($(this).attr('data-index'),10);
	    			var new_pending = $("#pending_input_"+index_data).val();

	    			var timeline_data = data_data['timeline'];
	    			timeline_data[index_data]['pending'] = new_pending;
	    			$("#span_"+index_data).html(new_pending);
	    			data_data['timeline'] = timeline_data;
	    			$("#cancel_"+index_data).trigger('click');

	    		});
	    		$(".cancel_index").on('click',function(){
	    			var index_data = parseInt($(this).attr('data-index'),10);
	    			$("#edit_"+index_data).show();
	    			$("#delete_"+index_data).show();
	    			$("#span_"+index_data).show();

	    			$("#save_"+index_data).hide();
	    			$("#cancel_"+index_data).hide();

	    			$("#pending_input_"+index_data).hide();	   

	    		});
	    		$(".edit_index").on('click',function(){
	    			var index_data = parseInt($(this).attr('data-index'),10);
	    			$("#edit_"+index_data).hide();
	    			$("#delete_"+index_data).hide();
	    			$("#span_"+index_data).hide();

	    			$("#save_"+index_data).show();
	    			$("#cancel_"+index_data).show();

	    			$("#pending_input_"+index_data).show();	    			

	    			$(this).hide();
	    		});
	    		$("#add_record").on('click',function(){
	    		 	if($("#link_hidden").val()=="") return;

	    		 	var hms = $("#pending_time").val();
					var a = hms.split(':');

					var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 

					if(seconds<=0) return;
					
	    		 	var data = '<tr>';
	    		 		data +='<td>' + $("#current_index").val() +'</td>';
	    		 		data +='<td>' + $("#link_hidden").val() +'</td>';
	    		 		data +='<td>' + $("#cloud_name_hidden").val() +'</td>';
	    		 		data +='<td>' + seconds +'</td>';
	    		 	data +='</tr>';
	    		 	$("#list-data").append(data);

		    		var timeline_data = data_data['timeline'];

		    		var data_current = {
		    			cloudName: $("#cloud_name_hidden").val(),
						link: $("#link_hidden").val(),
						pending: seconds,						
		    		};

		    		timeline_data[$("#current_index").val()] = data_current;
		    		data_data['timeline'] = timeline_data;

		    		$("#json_data").val(JSON.stringify(data_data));
		    		
		    		$("#current_index").val(parseInt($("#current_index").val(),10)+1);

		    		$("#cloud_name_hidden").val("");
		    		$("#cloud_name").html("&nbsp;");

		    		$("#link_hidden").val("");
		    		$("#link").html("&nbsp;");

	    		 });
	    	});
	    	function click_ne(data,data2){
	    		$("#cloud_name_hidden").val(data2);
	    		$("#cloud_name").html(data2);

	    		$("#link_hidden").val(data);
	    		$("#link").html(data);
	    		
	    	}
	    </script>
	</body>
</html>
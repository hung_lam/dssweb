<?php
function write_play_cloudstore_path($path){
    $fp = fopen('path.txt','w');
    fwrite($fp,$path);
    fclose($fp);
}
function addArrayFiles($filename,$dir){
    $arr_tmp = array();
    $arr_tmp['filename'] = $filename;
    $arr_tmp['folder'] = $dir;
    $arr_tmp['modified_date'] = (int)filemtime($dir."/".$filename);
    $arr_tmp['file_size'] = (int)filesize($dir."/".$filename);
    $arr_tmp['modified_to_date'] = date ("F d Y H:i:s.", filemtime($dir."/".$filename) );
    return $arr_tmp;
}
function GetInfoFiles($dir,$arr,$mod="../sync"){
    $files = scandir($dir, 1);
    foreach ($files as $key => $value) {
        $count = count(explode(".", $value));
        if($value=="." || $value==".." || $value=="desktop.ini"){
        }else if($count==1){
            $subdir = $dir."/".$value;
            $arr = GetInfoFiles($subdir,$arr,$mod);
        }else{
            $keyfile = str_replace(" ","_",$dir."/".$value);
            $keyfile = str_replace($mod."/","",$keyfile);
            $arr[$keyfile] = addArrayFiles($value,$dir);
        }

    }
    return $arr;
}
if(isset($_POST['cloudResource'])){
    // $fp = fopen('../../../../cloud.txt','w');
    // fwrite($fp,$_POST['cloudResource']);
    // fclose($fp);

    $fp = fopen('../../../cloud.txt','w');
    fwrite($fp,$_POST['cloudResource']);
    fclose($fp);

}
if(file_exists('../../../../cloud.txt')){
    $cloudResource = file_get_contents('../../../../cloud.txt');            
    $sync_folder = '../../../../'.($cloudResource!=''?$cloudResource:'') . '/sync';
}else{
    $cloudResource = file_get_contents('../../../cloud.txt');    
    $sync_folder = '../../../'.($cloudResource!=''?$cloudResource:'') . '/sync';
}
$sync_folder_cloud_store = 'play_'.str_replace(' ', '_', $cloudResource);
// write_play_cloudstore_path($sync_folder_cloud_store);
if(!file_exists(getcwd().'/'.$sync_folder_cloud_store)) @mkdir(getcwd().'/'.$sync_folder_cloud_store,0777);
$backup_folder = getcwd()."/play/backup";
if(!file_exists($backup_folder)) @mkdir($backup_folder,0777);
if(!file_exists(getcwd().'/'.$sync_folder_cloud_store."/media")) @mkdir(getcwd().'/'.$sync_folder_cloud_store."/media",0777);
$arr_sync = GetInfoFiles($sync_folder,array(),$sync_folder);
$arr_play = GetInfoFiles($sync_folder_cloud_store,array(),$sync_folder_cloud_store);
$n=0;
foreach ($arr_sync as $key => $value) {
    $filename = $value['folder']."/".$value['filename'];
    $path = str_replace($sync_folder, $sync_folder_cloud_store,$filename);
    $arr_path = explode("/", $path);
    $new_path = '/';
    for($i=0;$i<count($arr_path)-1;$i++){
        $new_path .=$arr_path[$i]."/";
        if(!file_exists(getcwd().$new_path)) @mkdir(getcwd().$new_path,0777);
    }
    $new_index = str_replace("Google_Drive", "Google Drive",$filename);
    $new_index = str_replace("One_Drive", "OneDrive",$new_index);
    $new_index = str_replace("Drop_box", "Dropbox",$new_index);
    $new_index = str_replace($sync_folder."/", "",$new_index);    
    $new_index = str_replace(" ", "_",$new_index);        


    $check_replace = str_replace($sync_folder, "play",$filename);      
    if( !isset($arr_play[$new_index]) || $value['file_size']!=$arr_play[$new_index]['file_size'] || $value['modified_date']>$arr_play[$new_index]['modified_date']){
        if(file_exists(getcwd().'/'.$check_replace)) {
            $check_replace = str_replace($sync_folder, "play",$filename);         
            $folder_date = date('d-m-y');
            $backup_folder = $backup_folder.'/'.$folder_date;
            @mkdir($backup_folder,0777);
            $backup_folder .= '/play';
            @mkdir($backup_folder,0777);
            $v_new_file_play = getcwd().'/'.str_replace($sync_folder_cloud_store, "/play/backup/".$folder_date.'/play',$check_replace);            
            @rename(getcwd().'/'.$check_replace, $v_new_file_play);
        }        
        copy($filename,str_replace($sync_folder, $sync_folder_cloud_store,$filename));
        $n++;
    }
}
echo $n;
<?php
session_start();
require_once 'app/src/Google/autoload.php';
$client_id = '121223727445-vsn048ep7mgte5jhmejhslt44mpc3efu.apps.googleusercontent.com'; //Client ID
$service_account_name = '121223727445-vsn048ep7mgte5jhmejhslt44mpc3efu@developer.gserviceaccount.com'; //Email Address
$key_file_location = 'app/DigitalSignage-618bc4083deb.p12'; //key.p12

if (strpos($client_id, "googleusercontent") == false
    || !strlen($service_account_name)
    || !strlen($key_file_location)) {
  echo missingServiceAccountDetailsWarning();
  exit;
}

$client = new Google_Client();
$client->setApplicationName("DSS Box");
$service = new Google_Service_Drive($client);


if (isset($_SESSION['service_token'])) {
  $client->setAccessToken($_SESSION['service_token']);
}
$key = file_get_contents($key_file_location);
$cred = new Google_Auth_AssertionCredentials(
    $service_account_name,
    array('https://www.googleapis.com/auth/drive'),
    $key
);
$client->setAssertionCredentials($cred);
if ($client->getAuth()->isAccessTokenExpired()) {
  $client->getAuth()->refreshTokenWithAssertion($cred);
}
$_SESSION['service_token'] = $client->getAccessToken();


if(isset($_POST['folder_id'])){
	$folder_id = $_POST['folder_id'];
	$folder_id = explode("?id=", $folder_id);
	$folder_id = end($folder_id);
	$folder_id = explode("&", $folder_id);
	$folder_id = $folder_id[0];

	$file = $service->files->get($folder_id);
	// echo "<pre>";
	// print_r($file->title);
	
	$setting = file_get_contents("datas/bootstrap.json");
	$setting = json_decode($setting, true);
	$setting['ResourceFolder'] = $file->title;
	if(isset($_POST['banner_w']) && isset($_POST['banner_h'])){
		$setting['SizeMode'] = 'custom';
		$setting['BannerWidth'] = (int)$_POST['banner_w'];
		$setting['BannerHeight'] = (int)$_POST['banner_h'];
		$setting['DownloadTime'] = (int)$_POST['download_time'];
	}else{
		$setting['SizeMode'] = 'auto';
		$setting['BannerWidth'] = 500;
		$setting['BannerHeight'] = 500;
		$setting['DownloadTime'] = 400;
	}
	$setting['FolderId'] = $folder_id;

	$contents = json_encode($setting);
	file_put_contents("datas/bootstrap.json",$contents);
	
	echo "Package setup is saved.</br></br>";


	$actual_link = $_SERVER['PHP_SELF'];
	$tmp = explode("/", $actual_link);
	$actual_link = str_replace(end($tmp),"",$actual_link);
	$actual_link = 'http://'.$_SERVER['HTTP_HOST'].$actual_link;
}

//https://drive.google.com/folderview?id=0B2gTqx8Dlg-vRFNkOEM1QkVjZEU&usp=sharing

?>
<form action="" method="POST">
Google Driver Folder ID <input type="text" value="<?php if(isset($_POST['folder_id'])) echo $_POST['folder_id'];?>" name="folder_id" style="width:700px;" /> </br></br>
Banner Width <input type="text" value="<?php echo isset($setting['BannerWidth'])? $setting['BannerWidth']:'';?>" name="banner_w" />px </br></br>
Banner Height <input type="text" value="<?php echo isset($setting['BannerHeight'])? $setting['BannerHeight']:'';?>" name="banner_h" />px </br></br>
Check Download Time <input type="text" value="<?php echo isset($setting['DownloadTime'])? $setting['DownloadTime']:'';?>" name="download_time" />seconds </br></br>
<input type="submit" value="Save" />
</form>




<br><br><br><br><br>
<a href="<?php echo $actual_link;?>">Run Packages</a><br>
<a href="tools.php">Home tool</a><br>
<a href="setup_packages.php">Setup Packages</a><br>
<a href="update_packages.php">Update Packages</a><br>
<a href="down_file.php">Download file</a>
<br><br><br><br>
<b>Email system:</b> 
121223727445-vsn048ep7mgte5jhmejhslt44mpc3efu@developer.gserviceaccount.com<br>
(Share your folder with this email)
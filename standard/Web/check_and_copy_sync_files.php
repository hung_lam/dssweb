<?php
function addArrayFiles($filename,$dir,$extened_folders = false,$origin_folder=''){
    $arr_tmp = array();
    $arr_tmp['filename'] = $filename;
    if($extened_folders) {
        $arr_tmp['extented_folder'] = $origin_folder;
    }
    $arr_tmp['folder'] = $dir;
    $arr_tmp['modified_date'] = filemtime($dir."/".$filename);
    $arr_tmp['file_size'] = filesize($dir."/".$filename);
    $arr_tmp['modified_to_date'] = date ("F d Y H:i:s.", filemtime($dir."/".$filename) );
    return $arr_tmp;
}
function GetInfoFiles($dir,$arr,$mod="../sync",$extened_folders = false,$origin_folder = '',$folder_name = '',$time_created_folder=''){
    $files = scandir($dir, 1);
    foreach ($files as $key => $value) {       
        $count = count(explode(".", $value));
        if($value=="." || $value==".." || $value=="desktop.ini"){
        }else if($count==1){
            $subdir = $dir."/".$value;            
            if(strpos($value, '(')!== false && strpos($value, ')') !== false){
                $extened_folders = true;
                $arr_current_folder_name = explode('(', $value);           
                $origin_folder = trim($arr_current_folder_name[0]);
            }else{
                $origin_folder = '';
                $extened_folders = false;
            }
            $time_created_folder = date ("d-m-Y", filemtime($dir.'/'.$value));
            $arr = GetInfoFiles($subdir,$arr,$mod,$extened_folders,$origin_folder,$value,$time_created_folder);
        }else{
            $keyfile = str_replace(" ","_",$dir."/".$value);
            $keyfile = str_replace($mod."/","",$keyfile);            
            $arr[$keyfile] = addArrayFiles($value,$dir,$extened_folders,$origin_folder);
            $arr[$keyfile]['folder_name'] = $folder_name;
            $arr[$keyfile]['folder_created_time'] = $time_created_folder;
            if(strpos($value, '(')!== false && strpos($value, ')') !== false){
                $file_name = (substr_replace($value, '', strpos($value, '('), (strpos($value, ')') - strpos($value, '(') +1) )) ;
                $file_name = substr_replace($file_name, '', strpos($value, ' '), 1) ;                
                $arr_current_folder_name = explode('(', $value);                               
                $origin_file_name = trim($arr_current_folder_name[0]);
                $arr[$keyfile]['file_name_change_name'] = $file_name;
            }
        }
    }
    
    return $arr;
}
$cloudResource = file_get_contents('../../../cloud.txt');
$sync_folder = '../../../'.($cloudResource!=''?$cloudResource:'') . '/sync';
if(!file_exists(getcwd()."/play")) @mkdir(getcwd()."/play",0777);
$backup_folder = getcwd()."/play/backup";
if(!file_exists($backup_folder)) @mkdir($backup_folder,0777);
if(!file_exists(getcwd()."/play/media")) @mkdir(getcwd()."/play/media",0777);
$arr_sync = GetInfoFiles($sync_folder,array(),$sync_folder);
$arr_sync = reCheckFolderCloudStore($arr_sync);
$arr_play = GetInfoFiles("play",array(),"play");
$n=0;
// echo '<pre>';
// print_r($arr_sync);
// die;
foreach ($arr_sync as $key => $value) {
    
    $filename = $value['folder']."/".$value['filename'];
    $path = str_replace($sync_folder, "play",$filename);
    $arr_path = explode("/", $path);
    $new_path = '/';
    for($i=0;$i<count($arr_path)-1;$i++){
        $new_path .=$arr_path[$i]."/";
        if(!file_exists(getcwd().$new_path)) @mkdir(getcwd().$new_path,0777);
    }
    $new_index = str_replace("Google_Drive", "Google Drive",$filename);
    $new_index = str_replace("One_Drive", "OneDrive",$new_index);
    $new_index = str_replace("Drop_box", "Dropbox",$new_index);
    $new_index = str_replace($sync_folder."/", "",$new_index);    
    $new_index = str_replace(" ", "_",$new_index);        

    $check_replace = str_replace($sync_folder, "play",$filename);      
    if( !isset($arr_play[$new_index]) || (int)$value['file_size'] != (int)$arr_play[$new_index]['file_size'] || (int)$value['modified_date']>(int)$arr_play[$new_index]['modified_date']){        
        copy($filename,str_replace($sync_folder, "play",$filename));        
        $n++;
    }
}
$arr_folder_rename = array();
foreach ($arr_sync as $key => $value) {
    if(isset($value['extented_folder'])){
        $arr_folder_rename[$value['folder_name']] = $value['extented_folder'];
    }
    if(isset($value['file_name_change_name'])){
        $v_new_name = $value['file_name_change_name'];
        $v_old_name = $value['filename'];
        $path = $value['folder'].'/';
        @rename ($path.$v_new_name,$path.$v_new_name.'_'.date('d-m-Y'));
        @rename ($path.$v_old_name,$path.$v_new_name);
    }
}
$path = getcwd().'/play/';
foreach ($arr_folder_rename as $change => $remove) {
    @rename ($path.$remove,$path.$remove.'_'.date('d-m-Y'));
    @rename ($path.$change,$path.$remove);
}

function write_file($text)
{
    $fp = fopen(getcwd().'/xem.txt','a+');
    fwrite($fp,$text);
    fclose($fp);
}
function reCheckFolderCloudStore($arr_data){
    $arr_new_data = array();
    $arr_old_data = array();
    $arr_extended_folder = array();
    $v_created_time_of_extented_folder = '';
    $v_created_time_of_origin_folder = '';   
    foreach($arr_data as $arr => $val){
        if(isset($val['extented_folder'])){
            $arr_extended_folder [$val['extented_folder']] = array(
                'folder_name'=>$val['extented_folder']
                ,'folder_created_time'=>$val['folder_created_time']
                ,'extented_folder'=>$val['extented_folder']
            );
        }
    }
    if(count($arr_extended_folder)){
        $arr_new_extended_folder = array();
        foreach($arr_extended_folder as $i => $current){
            $v_origin_folder_name = $current['folder_name'];            
            $v_extended_created_time = $current['folder_created_time'];
            $extented_folder = $current['extented_folder'];
            $v_ogigin_time = '';
            $arr_unset_path = array();
            $arr_change_folder_and_file_name = array();            
            foreach($arr_data as $arr => $val){                
                if($val['folder_name']!='' && $val['folder_name']==$v_origin_folder_name){                    
                    $v_ogigin_time = $val['folder_created_time'];                    
                    $arr_unset_path[] = $arr;
                }  
            }            
            if($v_ogigin_time!='' && strtotime($v_ogigin_time) <= strtotime($v_extended_created_time))
            {
                $arr_new_extended_folder [] = array(
                    'index_unset'=>$arr_unset_path
                );
            }
        }
        $arr_new_data = array();
        for($i=0;$i<count($arr_new_extended_folder);$i++){
            $arr_current_data = array();
            $arr_data_unset = $arr_new_extended_folder[$i]['index_unset'];
            foreach($arr_data as $arr => $val){
                if(!in_array($arr, $arr_data_unset)){
                    $arr_new_data[$arr] = $val;
                }
            }            
        }
        return $arr_new_data;
    }
    return $arr_data;
}
echo $n;
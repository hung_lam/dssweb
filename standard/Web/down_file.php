<?php
session_start();
require_once 'app/src/Google/autoload.php';
$client_id = '121223727445-vsn048ep7mgte5jhmejhslt44mpc3efu.apps.googleusercontent.com'; //Client ID
$service_account_name = '121223727445-vsn048ep7mgte5jhmejhslt44mpc3efu@developer.gserviceaccount.com'; //Email Address
$key_file_location = 'app/DigitalSignage-618bc4083deb.p12'; //key.p12

if (strpos($client_id, "googleusercontent") == false
    || !strlen($service_account_name)
    || !strlen($key_file_location)) {
  echo missingServiceAccountDetailsWarning();
  exit;
}

$client = new Google_Client();
$client->setApplicationName("DSS Box");
$service = new Google_Service_Drive($client);


if (isset($_SESSION['service_token'])) {
  $client->setAccessToken($_SESSION['service_token']);
}
$key = file_get_contents($key_file_location);
$cred = new Google_Auth_AssertionCredentials(
    $service_account_name,
    array('https://www.googleapis.com/auth/drive'),
    $key
);
$client->setAssertionCredentials($cred);
if ($client->getAuth()->isAccessTokenExpired()) {
  $client->getAuth()->refreshTokenWithAssertion($cred);
}
$_SESSION['service_token'] = $client->getAccessToken();

function CheckMakeFolder($filename){
  $arr_folder = explode("/", $filename);
  $fd = '';
  for($i=0;$i<count($arr_folder)-1;$i++){
    $fd .= $arr_folder[$i]."/";
    if (!file_exists($fd) && $arr_folder[$i]!="..") {
        mkdir($fd, 0777);
    }
  }
}
function downloadUrl($service,$downloadUrl,$filename) {
   CheckMakeFolder($filename);
   $request = new Google_Http_Request($downloadUrl, 'GET', null, null);
      $httpRequest = $service->getClient()->getAuth()->authenticatedRequest($request);
      if ($httpRequest->getResponseHttpCode() == 200) {
       $contents = $httpRequest->getResponseBody();
          file_put_contents($filename,$contents);
          return true;
      }
    return false;
}


$filename = 'datas/file_link.json';
$hasfile = false;
if(file_exists($filename)){
  $gd_file = file_get_contents($filename);
  $gd_file = json_decode($gd_file, true);
  //manual
  if(isset($_POST['fileid'])){
    $file_id = $_POST['fileid'];
    $file = $gd_file[$file_id];
    if(downloadUrl($service,$gd_file[$file_id]['downloadUrl'],"../sync/".$gd_file[$file_id]['title'])){
      copy("../sync/".$gd_file[$file_id]['title'],"play/".$gd_file[$file_id]['title']);
      $gd_file[$file_id]['status'] = 'downloaded';
      $hasfile = true;
    }
        
  
  }else{ //auto
    foreach ($gd_file as $key => $value) {
      if($value['status']=='waiting'){
        if(downloadUrl($service,$value['downloadUrl'],"../sync/".$value['title'])){
          copy("../sync/".$value['title'],"play/".$value['title']);
          $gd_file[$key]['status'] = 'downloaded';
          $hasfile = true;
        }
        break;
      }
    }
  }

  $gd_file = json_encode($gd_file);
  file_put_contents($filename,$gd_file);
  if($hasfile)
    echo 'downloaded';
  else
    echo 'download_full';
}else
  echo "Not found file_link.json";



<?php

//Ket noi voi Google Drive
session_start();
require_once 'app/src/Google/autoload.php';
$client_id = '121223727445-vsn048ep7mgte5jhmejhslt44mpc3efu.apps.googleusercontent.com'; //Client ID
$service_account_name = '121223727445-vsn048ep7mgte5jhmejhslt44mpc3efu@developer.gserviceaccount.com'; //Email Address
$key_file_location = 'app/DigitalSignage-618bc4083deb.p12'; //key.p12

if (strpos($client_id, "googleusercontent") == false
    || !strlen($service_account_name)
    || !strlen($key_file_location)) {
  echo missingServiceAccountDetailsWarning();
  exit;
}

$client = new Google_Client();
$client->setApplicationName("DSS Box");
$service = new Google_Service_Drive($client);


if (isset($_SESSION['service_token'])) {
  $client->setAccessToken($_SESSION['service_token']);
}
$key = file_get_contents($key_file_location);
$cred = new Google_Auth_AssertionCredentials(
    $service_account_name,
    array('https://www.googleapis.com/auth/drive'),
    $key
);
$client->setAssertionCredentials($cred);
if ($client->getAuth()->isAccessTokenExpired()) {
  $client->getAuth()->refreshTokenWithAssertion($cred);
}
$_SESSION['service_token'] = $client->getAccessToken();


//Xu ly sau khi ket noi Google Drive

function retrieveAllFiles($service) {
  $result = array();
  $pageToken = NULL;
  do {
    try {
      $parameters = array();
      if ($pageToken) {
        $parameters['pageToken'] = $pageToken;
      }
      $files = $service->files->listFiles($parameters);

      $result = array_merge($result, $files->getItems());
      $pageToken = $files->getNextPageToken();
    } catch (Exception $e) {
      print "An error occurred: " . $e->getMessage();
      $pageToken = NULL;
    }
  } while ($pageToken);
  return $result;
}

function getChildId($link_id,$arr_folder,$arr){
    foreach ($arr_folder as $key => $value) {
        if(isset($value['parent_id']) && $value['parent_id'] ==$link_id){
            $arr['list'][$key] = $key;
            $arr['title'][$key] = $arr['title'][$link_id]."/".$value['title'];
            $arr = getChildId($key,$arr_folder,$arr);
        }
    }
    return $arr;
}

function CheckStatusFile($file_id, $file, $modifiedDate){
    $filename = 'datas/file_link.json';
    if(file_exists($file) && file_exists($filename)){
        $arr_file_old = file_get_contents($filename);
        $arr_file_old = json_decode($arr_file_old);
        if(isset($arr_file_old->{$file_id}) && (string)$arr_file_old->{$file_id}->modifiedDate==$modifiedDate)
            return true;
        else
            return false;
    }else
        return false;
}

//Doc setting
$setting = file_get_contents("datas/bootstrap.json");
$setting = json_decode($setting, true);

if(isset($setting['FolderId']) && $setting['FolderId']!=''){
    $arr_file = $arr_folder = array();
    $listFile = retrieveAllFiles($service);//lay danh sach cac file
    
    foreach ($listFile as $key => $value) {
        if($value->mimeType=='application/vnd.google-apps.folder'){ //chia file load folder
            $arr_folder[$value->id]['title'] = $value->title;
            $arr_folder[$value->id]['parent'] = 1;
            $arr_folder[$value->id]['parent_id'] = 1;
            if(isset($value->parents[0]) && $value->parents[0]->isRoot!=1){
                $arr_folder[$value->id]['parent'] = $value->parents[0]->parentLink;
                $tmp = explode("/", $arr_folder[$value->id]['parent']);
                $arr_folder[$value->id]['parent_id'] = end($tmp);
            }

        }else{ //chia file loai files
            $arr_file[$value->id]['title'] = $value->title;
            $arr_file[$value->id]['folderLocal'] = '';
            $arr_file[$value->id]['parent'] = 1;
            $arr_file[$value->id]['parent_id'] = 1;
            $arr_file[$value->id]['downloadUrl'] = $value->downloadUrl;
            $arr_file[$value->id]['modifiedDate'] = $value->modifiedDate;
            if(isset($value->parents[0])){
                $arr_file[$value->id]['parent'] = $value->parents[0]->parentLink;
                $tmp = explode("/", $arr_file[$value->id]['parent']);
                $arr_file[$value->id]['parent_id'] = end($tmp);
            }
        }
    }
    //lay danh sach thu muc con 
    $in_folder = array();
    $in_folder['list'][$setting['FolderId']] = $setting['FolderId'];
    $in_folder['title'][$setting['FolderId']] = $setting['ResourceFolder'];
    
    foreach ($arr_folder as $key => $value) {
        $in_folder = getChildId($setting['FolderId'],$arr_folder,$in_folder);
    }
    $main_file = array();
    //tim file va bo sung duong link
    foreach ($arr_file as $key => $value) {
        if(in_array($value['parent_id'], $in_folder['list'])){
            $main_file[$key] = $value;
            $main_file[$key]['folderLocal'] =  $in_folder['title'][$value['parent_id']];
            $main_file[$key]['status'] = "waiting";
        }
    }
    //Kiem tra tinh trang file
    foreach ($main_file as $ids => $value) {
        $file = "play/".$value['title'];
        if(CheckStatusFile($ids,$file,$value['modifiedDate'])){
            $main_file[$ids]['status'] = 'downloaded';
        }
    }
    //Luu thong tin file vao json
    $filename = 'datas/file_link.json';    
    $content = json_encode($main_file);
    file_put_contents($filename,$content);

}
$actual_link = $_SERVER['PHP_SELF'];
$tmp = explode("/", $actual_link);
$actual_link = str_replace(end($tmp),"",$actual_link);
$actual_link = 'http://'.$_SERVER['HTTP_HOST'].$actual_link;
?>
<style type="text/css">
    table, th, td {
        width: 80%;
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 15px;
    }
</style>

<table >
    <tr>
        <td><b>Filename</b></td>
        <td><b>Folder</b></td>
        <td><b>Status</b></td>
        <td><b>Download</b></td>
        <td><b>Modified Date</b></td>
        <td><b>Link down</b></td>
    </tr>
    <?php foreach ($main_file as $key => $value) {?>
    <tr>
        <td><?php echo $value['title'];?></td>
        <td><?php echo $value['folderLocal'];?></td>
        <td><span id="<?php echo $key;?>"><?php echo $value['status'];?></span></td>
        <td><input class="dowbt" type="button" value="Download" onclick="dowloadFile('<?php echo $key;?>')" /></td>
        <td><span id="date_<?php echo $key;?>"><?php echo $value['modifiedDate'];?></span></td>
        <td><?php echo $value['downloadUrl'];?></td>

    </tr>
    <?php }?>
</table>

<script src="app/js/jquery.min.js"></script>
<script type="text/javascript">
    function dowloadFile(fileid){
        $("input").prop('disabled', true);
        $.ajax({
          method: "POST",
          url: "down_file.php",
          data: { fileid: fileid},
          success:function(ret){
            $("#"+fileid).html(ret);
            $("input").prop('disabled', false);
          }
        });
    }
</script>

<br><br><br><br><br>
<a href="<?php echo $actual_link;?>">Run Packages</a><br>
<a href="tools.php">Home tool</a><br>
<a href="setup_packages.php">Setup Packages</a><br>
<a href="update_packages.php">Update Packages</a><br>
<a href="down_file.php">Download file</a>
<br><br><br><br>
<b>Email system:</b> 
121223727445-vsn048ep7mgte5jhmejhslt44mpc3efu@developer.gserviceaccount.com<br>
(Share your folder with this email)
// Var
var Clock,ClockCheckConnetion;
var point = 0;
var ConnetionStartus = false;
var Setting;
var ClockDownload, Downloading;
var CheckSyncing;
var DelayTime =0;
var delayCheck = 5000;
var countNew=0;
Bootrap();


function Bootrap(){
  //CheckConnetion();
  LoadSetting();
  CheckAndCopySysnFiles();
  LoadIndex();
  //CheckAndDownload();
}
function LoadSetting(){
  var d = new Date(); var ts = "?t="+d.getTime();
  $.getJSON("datas/bootstrap.json"+ts, function(result){    
      Setting = result;      
      if(Setting.ResourceFolder!=undefined){
        SetSize();        
        startTime();
      }
  });
}
function SetSize(){
  var h,w,newsize,margin;
  var isRotate = false;
  if(Setting.SizeMode!=undefined && Setting.SizeMode=="custom" && Setting.BannerWidth!=undefined && Setting.BannerHeight!=undefined){
      w = Setting.BannerWidth;
      h = Setting.BannerHeight;
  }else{
      w = $(window).width();
      h = $(window).height();
  }
  if(CheckRotate(w,h)){
      var tmp=w; w=h; h=tmp;
      isRotate = true;
  }
  var newsize = calSizeFitBox(w,h,$(window).width(),$(window).height());
  w = newsize['w'];
  h = newsize['h'];
  var margin = 0;
  if(isRotate){
    var tmp=w; w=h; h=tmp;
    margin = Math.round((Math.max(w,h) - Math.min(w,h))/2);
    // $("#mainFrame").addClass("container_rotate_90_negative");
  }
  $("#mainFrame").attr("width",w);
  $("#mainFrame").attr("height",h);
  $("#mainFrame").css("width",w+"px");
  $("#mainFrame").css("height",h+"px");
  $("#mainFrame").css("margin-left",margin+"px");
  $("#mainFrame").css("margin-top","-"+margin+"px");
}
function LoadIndex(){
    var d = new Date(); var ts = "?t="+d.getTime();
    $("#mainFrame").attr("src","play/index.html?t="+ts);
}
function calSizeFitBox(slide_w,slide_h,box_w,box_h){
    var ret = {};
    if(box_w/box_h > slide_w/slide_h){ //fit width
        ret['w'] = box_w;
        ret['h'] = Math.round(slide_h/(slide_w/box_w));
        ret['scale'] = slide_w/box_w;
        ret['scale_by'] = 'W';
    }else{
        ret['w'] = Math.round(slide_w/(slide_h/box_h));
        ret['h'] = box_h;
        ret['scale'] = slide_h/box_h;
        ret['scale_by'] = 'H';
    }
    return ret;
}
function CheckRotate(w,h){
  var device = Orientation($(window).width(),$(window).height());
  var slide  = Orientation(w,h);
  if(device==slide)
    return false;
  else
    return true;
}
function Orientation(w,h){
    if(w>h) return 'v'; else return 'h';
}
function launchIntoFullscreen(element) {
  $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e){
      if(document.exitFullscreen && $("#FullcsreenButon").css("display")=="none" && document.fullscreenElement === null) {
          $("#FullcsreenButon").css("display","block")
      } else if(document.mozCancelFullScreen && $("#FullcsreenButon").css("display")=="none" && document.mozFullScreenElement === null) {
          $("#FullcsreenButon").css("display","block")
      } else if(document.webkitExitFullscreen && $("#FullcsreenButon").css("display")=="none" && document.webkitFullscreenElement === null) {
          $("#FullcsreenButon").css("display","block")
      }
  });
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if(element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}
function exitFullscreen() {
  if(document.exitFullscreen) {
    document.exitFullscreen();
  } else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}
function startTime() {
    //Count Main
    if(DelayTime>parseInt(Setting.DownloadTime)){
        DelayTime = 0;
    }
    DelayTime++;

    //loading dots
    if(point>0){
        var strp = "";
        for (var i = 0; i < point; i++) {
          strp += '.';
        };
        for (var i = point; i < 3; i++) {
          strp += "&nbsp";
        }
        $("#point").html(strp);
        if(point==3)
          point = 1;
        else
          point++;
    }
    $("#clock_time").css("display","block");
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    // add a zero in front of numbers<10
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById("clock_time").innerHTML = h + ":" + m + ":" + s;
    var t = setTimeout(function(){ startTime() }, 500);
}
function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
function CheckConnetion(){
  PingHost();
  console.log("ConnetionStartus=",ConnetionStartus);
  ClockCheckConnetion = setTimeout(function(){
    CheckConnetion();
  },50000);
}
function PingHost(){
  $.ajax({
      url: "http://query.yahooapis.com/v1/public/yql",
      jsonp: "callback",
      dataType: "jsonp",
      data: {
          q: "select title,abstract,url from search.news where query=\"cat\"",
          format: "json"
      },
      timeout: 10000,
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        if(textStatus==="timeout") {
            ConnetionStartus = false;
            SetSize();
        }
      },
      success: function( response ) {
          ConnetionStartus = true;
          SetSize();
      }
  });
}

function CheckAndCopySysnFiles(){
  if(delayCheck==undefined || delayCheck<1000)
      delayCheck = 5000;
  console.log(ConnetionStartus,"Checking Sync Files ..............",DelayTime);
  if(!CheckSyncing && DelayTime<100){
    CheckAndCopyAjax();
  }
  ClockDownload = setTimeout(function(){
      CheckAndCopySysnFiles();
  },delayCheck);  
}
function CheckAndCopyAjax(){
  if(Setting == undefined) return ;
  CheckSyncing = true;  
  console.log(ConnetionStartus," Check And Copy Sysn Files ..............");  
  $.ajax({
      method: "POST",
      url: "check_and_copy_sync_files.php",
      data: {
        type: 'auto'
        ,'cloudResource':Setting['cloudResource']
      },
      success:function(ret){        
        console.log('check copy sync file');
        console.log(ret);
        if(parseInt(ret)>0)
            Bootrap();
        CheckSyncing = false;
      }
  });
}

var LoadingCache=false;

function LoadCacheFiles(){
  if(delayCheck==undefined || delayCheck<1000)
      delayCheck = 5000;
  console.log(ConnetionStartus,"Loading Cache ..............",DelayTime);
  if(!LoadingCache && DelayTime<100){
    LoadCacheFilesAjax();
  }
  ClockDownload = setTimeout(function(){
      LoadCacheFiles();
  },delayCheck);  
}
function LoadCacheFilesAjax(){
  LoadingCache = true;
  console.log(ConnetionStartus,"Loading Cache Files Ajax ..............");
  $.ajax({
      method: "POST",
      url: "load_cache_files.php",
      data: { 
        type: 'auto'
        ,'cloudResource':Setting.cloudResource
      },
      success:function(ret){
        console.log(ret);
        LoadingCache = false;
      }
  });
}


function CheckAndDownload(){
  if(delayCheck==undefined || delayCheck<1000)
    delayCheck = 5000;
  console.log(ConnetionStartus,"Checking download ..............",DelayTime);
  if(ConnetionStartus && !Downloading && DelayTime<100){ //tu 0 den 100 la cho phep down load, tu 100 tro len ko cho phep download
    CheckUpdatePackages(function(){
      dowloadFile();
      console.log("Downloading ..............");
    });
  }
  ClockDownload = setTimeout(function(){
      CheckAndDownload();
    },delayCheck);  
}
function CheckUpdatePackages(callBack){
  console.log(ConnetionStartus,"Checking Update Packages ..............");
  $.ajax({
      method: "POST",
      url: "update_packages.php",
      data: { type: 'auto'},
      success:function(ret){
        callBack(); 
      }
    });
}
function dowloadFile(){
  Downloading = true;
    $.ajax({
      method: "POST",
      url: "down_file.php",
      data: { type: 'auto'},
      success:function(ret){
        Downloading = false;
        if(ret!='download_full'){
          DelayTime = 0;
          countNew = 1;
        }else{
          DelayTime = 100;
          delayCheck = 10000;
          if(countNew>0){
            countNew = 0;
            Bootrap();
          }
        }        
      }
    });
}

function rotateWindow(){
    if( $("#mainFrame").hasClass('container_rotate_vertical')){
        $("#mainFrame").removeClass('container_rotate_vertical');
        $("#mainFrame").addClass('container_rotate_90');
    }else if( $("#mainFrame").hasClass('container_rotate_90')){
        $("#mainFrame").removeClass('container_rotate_90');
        $("#mainFrame").addClass('container_rotate_vertical_negative');
    }else if( $("#mainFrame").hasClass('container_rotate_vertical_negative')){
        $("#mainFrame").removeClass('container_rotate_vertical_negative');
        $("#mainFrame").addClass('container_rotate_90_negative');
    }else if( $("#mainFrame").hasClass('container_rotate_90_negative')){
        $("#mainFrame").removeClass('container_rotate_90_negative');
        $("#mainFrame").addClass('container_rotate_vertical');
    }else{
      $("#mainFrame").addClass('container_rotate_90');
    }
    SetSize();
}